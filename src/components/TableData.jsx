import React from 'react'
import { Container,Table,Button} from 'react-bootstrap'
import Item from './Item'


function TableData(props) {
  let data1 = props.data.find( ({id}) => id=== 1 );
  let data2 = props.data.find( ({id}) => id=== 2 );
  let data3 = props.data.find( ({id}) => id=== 3 );
  let data4 = props.data.find( ({id}) => id=== 4 );
   
    return (
<Container style={{marginBottom:100}}>
  <h1>Table</h1>
  <Table striped bordered hover size="sm">
    <thead>
      <tr>
        <th>#</th>
        <th>Food</th>
        <th>Amount</th>
        <th>Price</th>
        <th>Total</th> 
      </tr>
    </thead>
    <tbody>
      <tr  hidden={data1.amount===0}>
        <th>{data1.id}</th>
        <th>{data1.title}</th>
        <th>{data1.amount}</th>
        <th>{data1.price}$</th>
        <th>{data1.total}$</th> 
      </tr>
      <tr  hidden={data2.amount===0}>
        <th>{data2.id}</th>
        <th>{data2.title}</th>
        <th>{data2.amount}</th>
        <th>{data2.price}$</th>
        <th>{data2.total}$</th> 
      </tr>
      <tr  hidden={data3.amount===0}>
        <th>{data3.id}</th>
        <th>{data3.title}</th>
        <th>{data3.amount}</th>
        <th>{data3.price}$</th>
        <th>{data3.total}$</th> 
      </tr>
      <tr  hidden={data4.amount===0}>
        <th>{data4.id}</th>
        <th>{data4.title}</th>
        <th>{data4.amount}</th>
        <th>{data4.price}$</th>
        <th>{data4.total}$</th> 
      </tr>
      <tr>
        <th></th>
        <th ></th>
        <th ></th>
        <th ></th>
        <th style={{color:'red',fontSize:20}}><b style={{color:'darkBlue'}}>Total Price: </b>{data1.total+data2.total+data3.total+data4.total}<b style={{color:'black'}}>$</b></th> 
      </tr>
    </tbody>
</Table>
<Button variant="primary" size="lg" style={{height:40,margin:'auto',width:300}} block> Send </Button>
 </Container>
  )
}

export default TableData
